package com.noobs.maps.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by harsh on 03-02-2017.
 */
public class userId {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("result")
    @Expose
    private String result;

    public int getId() {
        return id;
    }

    public String getResult() {
        return result;
    }

}
