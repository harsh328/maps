package com.noobs.maps.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by harsh on 03-02-2017.
 */
public class nearbyUser {
    @SerializedName("curr_lat")
    @Expose
    private Double currLat;
    @SerializedName("curr_lng")
    @Expose
    private Double currLng;
    @SerializedName("dst_lat")
    @Expose
    private Double dstLat;
    @SerializedName("dst_lng")
    @Expose
    private Double dstLng;

    public Double getCurrLat() {
        return currLat;
    }

    public void setCurrLat(Double currLat) {
        this.currLat = currLat;
    }

    public Double getCurrLng() {
        return currLng;
    }

    public void setCurrLng(Double currLng) {
        this.currLng = currLng;
    }

    public Double getDstLat() {
        return dstLat;
    }

    public void setDstLat(Double dstLat) {
        this.dstLat = dstLat;
    }

    public Double getDstLng() {
        return dstLng;
    }

    public void setDstLng(Double dstLng) {
        this.dstLng = dstLng;
    }
}
