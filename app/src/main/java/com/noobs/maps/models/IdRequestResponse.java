package com.noobs.maps.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by harsh on 03-02-2017.
 */
public class IdRequestResponse {
    @SerializedName("results")
    private List<IdRequest> results;

    public List<IdRequest> getResults() {
        return results;
    }

    public void setResults(List<IdRequest> results) {
        this.results = results;
    }
}
