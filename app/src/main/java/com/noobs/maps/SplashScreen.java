package com.noobs.maps;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.noobs.maps.models.IdRequest;
import com.noobs.maps.models.userId;
import com.noobs.maps.rest.ApiClient;
import com.noobs.maps.rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends AppCompatActivity implements View.OnClickListener{

    Button regConfirm;
    EditText edName,edPass,edEmail,edPhone;
    SharedPreferences sh;
    SharedPreferences.Editor editor;

    ApiInterface service= ApiClient.getClient().create(ApiInterface.class);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        findId();
        sh = getSharedPreferences("ID", 0);
        editor = sh.edit();

        String phone = sh.getString("Phone number", "Not");

        if (phone.equals("Not")) {
            regConfirm.setOnClickListener(this);
        } else {
            //Toast.makeText(SplashScreen.this, "Already exists", Toast.LENGTH_LONG).show();
            Intent i = new Intent(SplashScreen.this, Options.class);
            startActivity(i);
            SplashScreen.this.finish();
        }

    }

    protected void findId() {
        edName = (EditText) findViewById(R.id.edName);
        edPass = (EditText) findViewById(R.id.edPass);
        edEmail = (EditText) findViewById(R.id.edEmail);
        edPhone = (EditText) findViewById(R.id.edNumber);
        regConfirm = (Button) findViewById(R.id.btnConfirm);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnConfirm:

                IdRequest userRequest=new IdRequest();
                userRequest.setEmail(edEmail.getText().toString());
                userRequest.setName(edName.getText().toString());
                userRequest.setPhoneno(edPhone.getText().toString());
                userRequest.setPassword(edPass.getText().toString());
//                userRequest.setLat(23.0);
//                userRequest.setLng(72.0);


                Call<userId> userIdCall=service.getUserId(userRequest);
                userIdCall.enqueue(new Callback<userId>() {
                    @Override
                    public void onResponse(Call<userId> call, Response<userId> response) {
                        if(response.isSuccessful()){
                            userId id;
                            id=response.body();

                            editor.putString("id",String.valueOf(id.getId()));
                            editor.putString("Phone number", edPhone.getText().toString());
                            editor.putString("Password", edPass.getText().toString());
                            editor.commit();

                            Intent i = new Intent(SplashScreen.this, Options.class);
                            startActivity(i);
                            SplashScreen.this.finish();
                        }
                        else{
                            Toast.makeText(SplashScreen.this,"Maybe just maybe you don't deserve the service.",Toast.LENGTH_LONG).show();
                        }

//                        Toast.makeText(SplashScreen.this, id.getId()+"", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<userId> call, Throwable t) {
                        Log.d("Registration Activity","onFailure: " + t.getMessage());
                    }
                });
                break;
        }
    }
}
