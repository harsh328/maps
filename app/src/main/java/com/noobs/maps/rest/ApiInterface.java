package com.noobs.maps.rest;

import com.noobs.maps.models.IdRequest;
import com.noobs.maps.models.IdRequestResponse;
import com.noobs.maps.models.nearbyUser;
import com.noobs.maps.models.userId;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by harsh on 03-02-2017.
 */
public interface ApiInterface {

    @POST("post.php")
    Call<userId> getUserId(@Body IdRequest idRequest);

    @POST("radius.php")
    Call<IdRequestResponse> getUser(@Body nearbyUser useRequest);
}
