package com.noobs.maps;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class Options extends AppCompatActivity implements View.OnClickListener {

    Button drive,ride;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        toolbar.setTitle("Home");
//        setSupportActionBar(toolbar);
        drive=(Button)findViewById(R.id.btnDrive);
        ride=(Button)findViewById(R.id.btnRide);
        ride.setOnClickListener(this);
        drive.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch(v.getId()){
            case R.id.btnDrive:
                i=new Intent(Options.this,driver.class);
                startActivity(i);
                break;
            case R.id.btnRide:
                i=new Intent(Options.this,Maps.class);
                startActivity(i);
                break;
        }
    }
}
