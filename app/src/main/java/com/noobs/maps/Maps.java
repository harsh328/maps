package com.noobs.maps;

import android.Manifest;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.noobs.maps.models.IdRequest;
import com.noobs.maps.models.IdRequestResponse;
import com.noobs.maps.models.nearbyUser;
import com.noobs.maps.models.userId;
import com.noobs.maps.rest.ApiClient;
import com.noobs.maps.rest.ApiInterface;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Maps extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    GoogleMap mGoogleMap;
    GoogleApiClient mGoogleApiClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Ride");
        setSupportActionBar(toolbar);
        if (googleServicesAvailable()) {
            initMap();
        } else {
            // No Google Maps Layout
        }
    }

    private void initMap() {
        MapFragment mapFragment=(MapFragment)getFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);
    }

    public boolean googleServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {
            Dialog dialog = api.getErrorDialog(this, isAvailable, 0);
            dialog.show();
        } else {
            Toast.makeText(this, "Cant connect to play services", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap=googleMap;
//        goToLocationZoom(-34,151,15,"temp");
        mGoogleApiClient=new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
    }

    Circle circle;
    Marker marker1,marker2;
    Polyline line;

    private void goToLocationZoom(double lat, double lng,float zoom) {
        LatLng loc = new LatLng(lat, lng);
        CameraUpdate update=CameraUpdateFactory.newLatLngZoom(loc,zoom);
//        mGoogleMap.moveCamera(update);
        mGoogleMap.animateCamera(update);
    }

    public void geoLocate(View view) throws IOException {
        mGoogleMap.clear();
        mGoogleMap.addMarker(new MarkerOptions()
                .title("Current location")
                .position(new LatLng(marker1.getPosition().latitude,marker1.getPosition().longitude)));
        circle = drawCircle(new LatLng(marker1.getPosition().latitude, marker1.getPosition().longitude));

        EditText locSearch=(EditText)findViewById(R.id.editText);
        String location=locSearch.getText().toString();

        Geocoder gc=new Geocoder(this);
        List<Address> list=gc.getFromLocationName(location,1);
        android.location.Address add=list.get(0);

        double lat=add.getLatitude();
        double lng=add.getLongitude();
        goToLocationZoom(lat,lng,15);
        setMarker(location, lat, lng);

        nearbyUser userFind=new nearbyUser();
        userFind.setCurrLat(marker1.getPosition().latitude);
        userFind.setCurrLng(marker1.getPosition().longitude);
        userFind.setDstLat(marker2.getPosition().latitude);
        userFind.setDstLng(marker2.getPosition().longitude);

        ApiInterface service= ApiClient.getClient().create(ApiInterface.class);

        Call<IdRequestResponse> call = service.getUser(userFind);
        call.enqueue(new Callback<IdRequestResponse>() {
            @Override
            public void onResponse(Call<IdRequestResponse> call, Response<IdRequestResponse> response) {
                List<IdRequest> user=response.body().getResults();
                for(IdRequest id:user){
                    MarkerOptions options = new MarkerOptions()
                            .title(id.getName()+", "+id.getPhoneno())
                            .position(new LatLng(id.getLat(), id.getLng()));
                    mGoogleMap.addMarker(options);
                }
            }

            @Override
            public void onFailure(Call<IdRequestResponse> call, Throwable t) {

            }
        });
    }

    private void setMarker(String locality, double lat, double lng){
        MarkerOptions options = new MarkerOptions()
                .title(locality)
                .position(new LatLng(lat, lng));
        if(marker2 == null) {
            marker2 = mGoogleMap.addMarker(options);
        } else {
            removeEverything();
            marker2 = mGoogleMap.addMarker(options);
        }
        drawLine();
    }

    private void removeEverything() {
        marker2.remove();
        marker2=null;
        line.remove();
    }

    private void drawLine() {
        PolylineOptions options = new PolylineOptions()
                .add(marker1.getPosition())
                .add(marker2.getPosition())
                .color(Color.BLUE)
                .width(6);

        line = mGoogleMap.addPolyline(options);
    }

    private Circle drawCircle(LatLng latLng) {

        CircleOptions options = new CircleOptions()
                .center(latLng)
                .radius(3000)
                .fillColor(0x330000FF)
                .strokeColor(Color.BLUE)
                .strokeWidth(3);

        return mGoogleMap.addCircle(options);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mapTypeNone:
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                break;
            case R.id.mapTypeNormal:
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case R.id.mapTypeSatellite:
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.mapTypeTerrain:
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
            case R.id.mapTypeHybrid:
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    LocationRequest mLocationRequest;
    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        mLocationRequest.setInterval(5000);
//        mLocationRequest.setNumUpdates(1);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest,this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if(location==null)
            Toast.makeText(this,"Can't get current location",Toast.LENGTH_LONG).show();
        else{
            Geocoder gc=new Geocoder(this);
            List<Address> list= null;
            try {
                list = gc.getFromLocation(location.getLatitude(),location.getLongitude(),1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            android.location.Address add=list.get(0);
            goToLocationZoom(location.getLatitude(),location.getLongitude(),15);
            MarkerOptions options = new MarkerOptions()
                    .title("Current location")
                    .position(new LatLng(location.getLatitude(), location.getLongitude()));
            marker1 = mGoogleMap.addMarker(options);
//            setMarker("Current location",location.getLatitude(),location.getLongitude());
            circle = drawCircle(new LatLng(location.getLatitude(), location.getLongitude()));


            SharedPreferences sh=getSharedPreferences("ID",0);

            ApiInterface service= ApiClient.getClient().create(ApiInterface.class);
            IdRequest userRequest=new IdRequest();
            userRequest.setId(Integer.parseInt(sh.getString("id","33")));
            userRequest.setLat(location.getLatitude());
            userRequest.setLng(location.getLongitude());
            Call<userId> userIdCall=service.getUserId(userRequest);
            userIdCall.enqueue(new Callback<userId>() {
                @Override
                public void onResponse(Call<userId> call, Response<userId> response) {
                    if(response.isSuccessful()){
                        userId id;
                        id=response.body();
                    }
                    else{
                        Toast.makeText(Maps.this,"Maybe just maybe you don't deserve the service.",Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void onFailure(Call<userId> call, Throwable t) {
                    Log.d("Registration Activity","onFailure: " + t.getMessage());
                }
            });
        }
    }
}
